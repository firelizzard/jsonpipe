package jsonpipe

import (
	"runtime"
	"sync/atomic"
)

// Adapted from https://github.com/golang/go/discussions/54245
// Originally by Ian Lance Taylor, modified by Ethan Reesor

func newGen[E any](gen func(yield func(E) bool) error) *genIter[E] {
	next := make(chan bool)
	value := make(chan E)

	it := &genIter[E]{value: value, next: next}
	runtime.SetFinalizer(it, (*genIter[E]).Stop)

	g := func() {
		defer close(value)

		// coroutine switch
		var z E
		value <- z
		if !<-next {
			return
		}

		it.err = gen(func(v E) bool {
			// coroutine switch
			value <- v
			return <-next
		})

		// coroutine switch
		close(value)
	}

	// coroutine switch
	go g()
	<-value

	return it
}

type genIter[E any] struct {
	value  chan E
	next   chan bool
	err    error
	closed atomic.Bool
}

func (it *genIter[E]) Next() (E, bool) {
	// coroutine switch
	it.next <- true
	v, ok := <-it.value
	return v, ok
}

func (it *genIter[E]) Stop() error {
	if !it.closed.CompareAndSwap(false, true) {
		return it.err // Do once
	}

	runtime.SetFinalizer(it, nil)

	// coroutine switch
	close(it.next)
	<-it.value

	return it.err
}
