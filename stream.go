package jsonpipe

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"strconv"
	"strings"
)

type Writer[T any] struct {
	wr io.Writer
}

func NewWriter[T any](wr io.Writer) *Writer[T] {
	return &Writer[T]{wr}
}

func (w *Writer[T]) Write(v T) error {
	data, err := json.Marshal(v)
	if err != nil {
		return err
	}

	_, err = fmt.Fprintf(w.wr, "Content-Length: %d\r\n\r\n%s\r\n", len(data)+2, data)
	return err
}

type Reader[T any] struct {
	*genIter[T]
}

func NewReader[T any](rd io.Reader) *Reader[T] {
	return NewReaderWith(rd, func(b []byte) (T, error) {
		var v T
		err := json.Unmarshal(b, &v)
		return v, err
	})
}

func NewReaderWith[T any](rd io.Reader, unmarshal func([]byte) (T, error)) *Reader[T] {
	return &Reader[T]{newGen(func(yield func(T) bool) error {
		var buf [1 << 10]byte
		var pos int
		headers := map[string]string{}
	read:
		for {
			n, err := rd.Read(buf[pos:])
			if err != nil {
				if errors.Is(err, io.EOF) {
					return nil
				}
				return err
			}
			if n == 0 {
				return errors.New("read zero bytes")
			}
			pos += n

		header:
			var i, j int
			for {
				j = bytes.Index(buf[i:pos], []byte("\r\n"))
				if j < 0 {
					continue read // Wait for more data
				}
				if j == 0 {
					i += 2
					break
				}

				parts := bytes.SplitN(buf[i:i+j], []byte(":"), 2)
				headers[strings.ToLower(strings.TrimSpace(string(parts[0])))] = strings.TrimSpace(string(parts[1]))
				i += j + 2
			}

			length, err := strconv.ParseUint(headers["content-length"], 10, 64)
			if err != nil {
				log.Printf("Content length is missing or invalid: %q", headers["content-length"])
				copy(buf[:], buf[i:pos])
				pos -= i
				continue
			}

			b := make([]byte, length)
			n = copy(b, buf[i:pos])
			i += n
			if uint64(n) < length {
				_, err = io.ReadFull(rd, b[n:])
				if err != nil {
					return err
				}
			}

			v, err := unmarshal(b)
			if err != nil {
				return err
			}

			if !yield(v) {
				return nil
			}

			copy(buf[:], buf[i:pos])
			pos -= i
			if pos > 0 {
				goto header
			}
		}
	})}
}
